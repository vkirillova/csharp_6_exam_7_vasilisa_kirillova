﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExamMVCApp.Models
{
    public class OrderCreateModel
    {
        [Display(Name = "Product")]
        public int ProductId{ get; set; }
        public SelectList ProductName{ get; set; }

        [Display(Name = "Brand")]
        public int? BrandId { get; set; }
        public SelectList ProductBrand { get; set; }
        
        [Display(Name = "Product's price")]
        public decimal ProductPrice { get; set; }

        [Display(Name = "Quantity of product")]
        [Required(ErrorMessage = "Field cannot be empty")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Quantity cannot be negative")]
        public int Quantity { get; set; }

        [Display(Name = "Customer's name")]
        [Required(ErrorMessage = "Field cannot be empty")]
        public string CustomerName { get; set; }

        [Display(Name = "Comment to order")]
        public string Comment { get; set; }

        public DateTime OrderDateTime { get; set; }

        public decimal OrderPrice()
        {
            return Quantity * ProductPrice;
        }
    }
}
