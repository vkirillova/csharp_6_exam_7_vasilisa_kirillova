﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExamMVCApp.Models
{
    public class ProductFilterModel
    {
        public string Name { get; set; }
        public int? CategoryId { get; set; }
        public int? BrandId { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }
        public List<ProductModel> Products { get; set; }
        public SelectList CategoriesSelect { get; set; }
        public SelectList BrandsSelect { get; set; }
    }
}
