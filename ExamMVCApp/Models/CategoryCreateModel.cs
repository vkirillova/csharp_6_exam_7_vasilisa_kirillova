﻿using System.ComponentModel.DataAnnotations;

namespace ExamMVCApp.Models
{
    public class CategoryCreateModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name cannot be empty")]
        public string Name { get; set; }
    }
}
