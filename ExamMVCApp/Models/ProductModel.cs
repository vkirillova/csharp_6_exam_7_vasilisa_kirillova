﻿namespace ExamMVCApp.Models
{
    public class ProductModel
    {
        public static string NoBrand = "NoName";

        public int Id { get; set; }
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
        public decimal Price { get; set; }
    }
}
