﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExamMVCApp.Models
{
    public class BrandCreateModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name cannot be empty")]
        public string Name { get; set; }
    }
}
