﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamMVCApp.Models
{
    public class OrderModel
    {
        public static string NoBrand = "NoName";
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public decimal ProductPrice { get; set; }
        public DateTime OrderDateTime { get; set; }
        public string CustomerName { get; set; }
        public decimal OrderPrice { get; set; }
        public string OrderComment { get; set; }
    }
}
