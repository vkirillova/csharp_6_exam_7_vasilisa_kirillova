﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExamMVCApp.Models
{
    public class ProductCreateModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Field cannot be empty")]
        public string Name { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Field cannot be empty")]
        public int CategoryId { get; set; }

        [Display(Name = "Brand")]
        public int? BrandId { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "Field cannot be empty")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Price cannot be negative")]
        public decimal Price { get; set; }
        public SelectList CategoriesSelect { get; set; }
        public SelectList BrandsSelect { get; set; }
    }
}
