﻿using AutoMapper;
using ExamMVCApp.DAL.Entities;
using ExamMVCApp.Models;

namespace ExamMVCApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateProductToProductEditModel();
            CreateProductEditModelToProduct();

            CreateCategoryToCategoryModelMap();
            CreateCategoryCreateModelToCategory();

            CreateBrandToBrandModelMap();
            CreateBrandCreateModelToBrand();
            
            CreateOrderToOrderModelMap();
            CreateOrderCreateModelToOrder();
            
        }

        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }

        private void CreateBrandToBrandModelMap()
        {
            CreateMap<Brand, BrandModel>();
        }

        private void CreateCategoryCreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }

        private void CreateCategoryToCategoryModelMap()
        {
            CreateMap<Category, CategoryModel>();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                    src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }

        private void CreateProductToProductEditModel()
        {
            CreateMap<Product, ProductEditModel>();
        }

        private void CreateProductEditModelToProduct()
        {
            CreateMap<ProductEditModel, Product>();
        }

        private void CreateOrderCreateModelToOrder()
        {
            CreateMap<OrderCreateModel, Order>();
        }

        private void CreateOrderToOrderModelMap()
        {
            CreateMap<Order, OrderModel>()
                .ForMember(target => target.Brand,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.ProductName,
                    src => src.MapFrom(p => p.Product.Name))
                .ForMember(target => target.ProductPrice,
                    src => src.MapFrom(p => p.Product.Price));
        }
    }
}
