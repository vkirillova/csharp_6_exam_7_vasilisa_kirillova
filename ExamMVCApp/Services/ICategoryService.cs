﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamMVCApp.Models;

namespace ExamMVCApp.Services
{
    public interface ICategoryService
    {
        List<CategoryModel> Categories(CategoryFilterModel model);
        CategoryCreateModel GetCategoryCreateModel();
        void CreateCategory(CategoryCreateModel model);
    }
}
