﻿using System.Collections.Generic;
using ExamMVCApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExamMVCApp.Services
{
    public interface IProductService
    {
        List<ProductModel> SearchProducts(ProductFilterModel model);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
        SelectList GetCategoriesSelect();
        SelectList GetBrandsSelect();
        ProductEditModel GetProductById(int id);
        void EditProduct(ProductEditModel model);
    }
}