﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ExamMVCApp.DAL;
using ExamMVCApp.DAL.Entities;
using ExamMVCApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExamMVCApp.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrderService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<OrderModel> FilterOrders(OrderFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Order> orders = unitOfWork.Orders.GetAllWithProductsAndBrands();

                orders = orders
                    .ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByCustomerName(model.CustomerName);

                List<OrderModel> orderModels = Mapper.Map<List<OrderModel>>(orders);

                return orderModels;
            }
        }

        public OrderCreateModel GetOrderCreateModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                return new OrderCreateModel()
                {
                    ProductName = GetProductsSelect(),
                    ProductBrand = GetBrandsSelect(),
                    OrderDateTime = DateTime.Now,
                };
            }
        }

        public OrderCreateModel GetOrderProductCreateModel(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                return new OrderCreateModel()
                {
                    ProductId = id
                };
            }
        }

        public void CreateOrder(OrderCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var order = Mapper.Map<Order>(model);
                unitOfWork.Orders.Create(order);
            }
        }

        public SelectList GetProductsSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var products = unitOfWork.Products.GetAll().ToList();
                return new SelectList(products, nameof(Product.Id), nameof(Product.Name));
            }
        }

        public SelectList GetBrandsSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();
                return new SelectList(brands, nameof(Brand.Id), nameof(Brand.Name));
            }
        }
    }
}
