﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamMVCApp.Models;

namespace ExamMVCApp.Services
{
    public interface IOrderService
    {
        List<OrderModel> FilterOrders(OrderFilterModel model);
        OrderCreateModel GetOrderCreateModel();
        OrderCreateModel GetOrderProductCreateModel(int id);
        void CreateOrder(OrderCreateModel model);
    }
}
