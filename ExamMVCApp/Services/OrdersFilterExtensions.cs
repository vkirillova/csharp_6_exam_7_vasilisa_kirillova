﻿using ExamMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamMVCApp.DAL;

namespace ExamMVCApp.Services
{
    public static class OrdersFilterExtensions
    {
        public static IEnumerable<Order> ByPriceFrom(this IEnumerable<Order> orders, decimal? priceFrom)
        {
            if (priceFrom.HasValue)
                return orders.Where(p => p.OrderPrice >= priceFrom.Value);
            return orders;
        }

        public static IEnumerable<Order> ByPriceTo(this IEnumerable<Order> orders, decimal? priceTo)
        {
            if (priceTo.HasValue)
                return orders.Where(p => p.OrderPrice <= priceTo.Value);
            return orders;
        }

        public static IEnumerable<Order> ByCustomerName(this IEnumerable<Order> orders, string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
                orders = orders.Where(p => p.CustomerName.Contains(name));
            return orders;
        }
    }
}
