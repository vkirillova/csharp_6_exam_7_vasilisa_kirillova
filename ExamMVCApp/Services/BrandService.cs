﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ExamMVCApp.DAL;
using ExamMVCApp.DAL.Entities;
using ExamMVCApp.Models;

namespace ExamMVCApp.Services
{
    public class BrandService: IBrandService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public BrandCreateModel GetBrandCreateModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                return new BrandCreateModel();
            }
        }

        public void CreateBrand(BrandCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brand = Mapper.Map<Brand>(model);

                unitOfWork.Brands.Create(brand);
            }
        }

        public List<BrandModel> Brands(BrandFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Brand> brands = unitOfWork.Brands.GetAll();
                List<BrandModel> brandModels = Mapper.Map<List<BrandModel>>(brands);

                return brandModels;
            }
        }
    }
}
