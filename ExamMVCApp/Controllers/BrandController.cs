﻿using System;
using ExamMVCApp.Models;
using ExamMVCApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace ExamMVCApp.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            if (brandService == null)
                throw new ArgumentNullException(nameof(brandService));
            _brandService = brandService;
        }

        public IActionResult Index(BrandFilterModel model)
        {
            var models = _brandService.Brands(model);
            return View(models);
        }

        public IActionResult Create()
        {
            var model = _brandService.GetBrandCreateModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BrandCreateModel model)
        {
            try
            {
                _brandService.CreateBrand(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}