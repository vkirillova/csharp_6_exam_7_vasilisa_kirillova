﻿using System;
using ExamMVCApp.Models;
using ExamMVCApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace ExamMVCApp.Controllers
{
    public class OrderController: Controller
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            if (orderService == null)
                throw new ArgumentNullException(nameof(orderService));

            _orderService = orderService;
        }

        public IActionResult Index(OrderFilterModel model)
        {
            try
            {
                var models = _orderService.FilterOrders(model);

                return View(models);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        public IActionResult CreateOrder()
        {
            var model = _orderService.GetOrderCreateModel();


            return View(model);
        }

        [HttpPost]
        public IActionResult CreateOrder(OrderCreateModel model)
        {
            try
            {
                _orderService.CreateOrder(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult ProductOrderCreate(int id)
        {
            var model = _orderService.GetOrderProductCreateModel(id);

            return View(model);
        }
    }
}
