﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExamMVCApp.DAL.Migrations
{
    public partial class ForeignKeyForBrandInOrders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Brands_ProductId",
                table: "Orders");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BrandId",
                table: "Orders",
                column: "BrandId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Brands_BrandId",
                table: "Orders",
                column: "BrandId",
                principalTable: "Brands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Brands_BrandId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_BrandId",
                table: "Orders");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Brands_ProductId",
                table: "Orders",
                column: "ProductId",
                principalTable: "Brands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
