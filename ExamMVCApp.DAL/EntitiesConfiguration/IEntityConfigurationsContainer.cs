﻿using ExamMVCApp.DAL.Entities;

namespace ExamMVCApp.DAL.EntitiesConfiguration
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Product> ProductConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Brand> BrandConfiguration { get; }
        IEntityConfiguration<Order> OrderConfiguration { get; }
    }
}
