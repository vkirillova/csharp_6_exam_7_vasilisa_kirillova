﻿using ExamMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamMVCApp.DAL.EntitiesConfiguration
{
    public class OrderConfiguration : BaseEntityConfiguration<Order>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Order> builder)
        {
            builder.Property(p => p.CustomerName)
                .HasMaxLength(500)
                .IsRequired();

            builder.Property(p => p.OrderPrice)
                .HasDefaultValue(0.0M)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Order> builder)
        {
            builder.HasOne(b=>b.Product)
                .WithMany(b=>b.Orders)
                .HasForeignKey(b => b.ProductId);

            builder.HasOne(b => b.Brand)
                .WithMany(b => b.Orders)
                .HasForeignKey(b => b.BrandId);

        }
    }
}
