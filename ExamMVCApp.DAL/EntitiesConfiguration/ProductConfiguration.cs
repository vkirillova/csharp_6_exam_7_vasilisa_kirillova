﻿using ExamMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamMVCApp.DAL.EntitiesConfiguration
{
    public class ProductConfiguration : BaseEntityConfiguration<Product>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Product> builder)
        {
            builder.Property(p => p.Name)
                .HasMaxLength(500)
                .IsRequired();

            builder.Property(p => p.Price)
                .HasDefaultValue(0.0M)
                .IsRequired();

            builder.HasIndex(b => b.Name).IsUnique(false);
        }
        
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Product> builder)
        {
            builder
                .HasOne(b => b.Category)
                .WithMany(b => b.Products)
                .HasForeignKey(b => b.CategoryId)
                .IsRequired();

            builder.HasOne(b => b.Brand)
                .WithMany(b => b.Products)
                .HasForeignKey(b => b.BrandId);

            builder.HasMany(b => b.Orders)
                .WithOne(b => b.Product)
                .HasForeignKey(b => b.ProductId);
        }
    }
}
