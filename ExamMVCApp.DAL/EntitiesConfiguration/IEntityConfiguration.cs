﻿using System;
using ExamMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamMVCApp.DAL.EntitiesConfiguration
{
    public interface IEntityConfiguration<T> where T : Entity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}