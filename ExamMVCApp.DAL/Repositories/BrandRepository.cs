﻿using ExamMVCApp.DAL.Entities;

namespace ExamMVCApp.DAL.Repositories
{
    public class BrandRepository : Repository<Brand>, IBrandRepository
    {
        public BrandRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Brands;
        }
    }
}