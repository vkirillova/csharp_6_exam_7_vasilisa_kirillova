﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExamMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace ExamMVCApp.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Orders;
        }

        public IEnumerable<Order> GetAllWithProductsAndBrandsByPrice(decimal priceFrom, decimal priceTo)
        {
            return GetAllWithProductsAndBrands()
                .Where(p => p.OrderPrice >= priceFrom && p.OrderPrice <= priceTo)
                .ToList();
        }

        public IEnumerable<Order> GetAllWithProductsAndBrands()
        {
            return entities
                .Include(e => e.Brand)
                .Include(e => e.Product)
                .ToList();
        }
    }
}
