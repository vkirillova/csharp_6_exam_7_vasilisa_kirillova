﻿using System.Collections.Generic;
using ExamMVCApp.DAL.Entities;

namespace ExamMVCApp.DAL.Repositories
{
    public interface IBrandRepository : IRepository<Brand>
    {
    }
}
