﻿using System.Collections.Generic;
using System.Linq;
using ExamMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace ExamMVCApp.DAL.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Products;
        }

        public Product GetTheMostExpensiveProduct()
        {
            return entities.OrderByDescending(e => e.Price).First();
        }

        public IEnumerable<Product> GetAllWithCategoriesAndBrandsByPrice(decimal priceFrom, decimal priceTo)
        {
            return GetAllWithCategoriesAndBrands()
                .Where(p => p.Price >= priceFrom && p.Price <= priceTo)
                .ToList();
        }

        public IEnumerable<Product> GetAllWithCategoriesAndBrands()
        {
            return entities
                .Include(e => e.Brand)
                .Include(e => e.Category)
                .ToList();
        }
    }
}