﻿using System.Collections.Generic;
using ExamMVCApp.DAL.Entities;

namespace ExamMVCApp.DAL.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        T Create(T entity);

        T GetById(int id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);
    }
}
