﻿using System.Collections.Generic;
using ExamMVCApp.DAL.Entities;

namespace ExamMVCApp.DAL.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetTheMostExpensiveProduct();
        IEnumerable<Product> GetAllWithCategoriesAndBrandsByPrice(decimal priceFrom, decimal priceTo);
        IEnumerable<Product> GetAllWithCategoriesAndBrands();
    }
}
