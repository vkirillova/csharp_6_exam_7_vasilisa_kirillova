﻿using ExamMVCApp.DAL.Entities;

namespace ExamMVCApp.DAL.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
