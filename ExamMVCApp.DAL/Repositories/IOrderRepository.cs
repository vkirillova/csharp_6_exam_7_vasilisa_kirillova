﻿using System;
using System.Collections.Generic;
using System.Text;
using ExamMVCApp.DAL.Entities;

namespace ExamMVCApp.DAL.Repositories
{
    public interface IOrderRepository: IRepository<Order>
    {
        IEnumerable<Order> GetAllWithProductsAndBrandsByPrice(decimal priceFrom, decimal priceTo);
        IEnumerable<Order> GetAllWithProductsAndBrands();
    }
}
