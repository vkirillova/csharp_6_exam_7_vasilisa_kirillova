﻿using ExamMVCApp.DAL.Entities;
using ExamMVCApp.DAL.EntitiesConfiguration;
using Microsoft.EntityFrameworkCore;

namespace ExamMVCApp.DAL
{
    public class ApplicationDbContext : DbContext
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Order> Orders { get; set; }
        
        public ApplicationDbContext(DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.ProductConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.CategoryConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.BrandConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.OrderConfiguration.ProvideConfigurationAction());

        }
    }
}
