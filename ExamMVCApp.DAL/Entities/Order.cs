﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamMVCApp.DAL.Entities
{
    public class Order: Entity
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int? BrandId { get; set; }
        public Brand Brand { get; set; }
        public decimal ProductPrice { get; set; }
        public DateTime OrderDateTime { get; set; }
        public string CustomerName { get; set; }
        public decimal OrderPrice { get; set; }
        public string OrderComment { get; set; }
    }
}
