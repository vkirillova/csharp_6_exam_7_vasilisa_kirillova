﻿using System.Collections.Generic;

namespace ExamMVCApp.DAL.Entities
{
    public class Brand : Entity
    {
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
