﻿using System.Collections.Generic;

namespace ExamMVCApp.DAL.Entities
{
    public class Category : Entity
    {
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
